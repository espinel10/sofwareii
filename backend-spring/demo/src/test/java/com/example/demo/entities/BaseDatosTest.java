package com.example.demo.entities;

import org.junit.Test;
/*
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import java.sql.SQLException;
import java.sql.Statement;
*/
import static org.junit.Assert.*;

public class BaseDatosTest {


    @Test
    public void validar_actualiza() {
        BaseDatos bd=new BaseDatos();
        Empleado e=new Empleado(10,"ASDASDDSA","ESPESAD","SADSDADS","ASDASDSD","US","CC","asdsad12223345","a@a.com","1-1-20215","ADMIN","ACTIVO");
        bd.actualizar_empleado(e,1);
        assertTrue(true);
    }

    @Test
    public void validar_create_empleado() {
        BaseDatos bd=new BaseDatos();
        int cont=100;

        Empleado e7=new Empleado(cont,"ASDSADSDA","ESPESAD","SADSDADS","ASDASDSD","US","CC","asdsad12223345","a@a.com","1-1-20215","ADMIN","ACTIVO");
        bd.create_empleado(e7);
        assertTrue(bd.getAll().size()==101);

        Empleado e8=new Empleado(cont,"ASDSADSDA","ESPESAD","SADSDADS","ASDASDSD","US","CC","asdsad12223345","a@a.com","1-1-20215","ADMIN","ACTIVO");
        bd.create_empleado(e8);
        assertTrue(bd.getAll().size()==101);

    }

    @Test
    public void validar_delete() {
        BaseDatos bd=new BaseDatos();
        bd.delete_empleado(1);
        assertTrue(bd.getAll().size()==99);

    }

    @Test
    public void validar_get_empleado(){
        BaseDatos bd=new BaseDatos();
        Empleado e7=new Empleado(bd.getAll().size()+1,"ASDSADSDA","ESPESAD","SADSDADS","ASDASDSD","US","CC","asdsad12223345","a@a.com","1-1-20215","ADMIN","ACTIVO");
        bd.create_empleado(e7);
        Empleado empleado=bd.get_empleado(bd.getAll().size());
        assertTrue(e7.equals(empleado));
    }


}