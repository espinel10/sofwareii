package com.example.demo.employe.model;

import java.util.Objects;

public class Employe {
    public int id;
    public String fLastName;
    public String sLastName;
    public String fName;
    public String sName;
    public String contry;

    public void setId(int id) {
        this.id = id;
    }

    public void setfLastName(String fLastName) {
        this.fLastName = fLastName;
    }

    public void setsLastName(String sLastName) {
        this.sLastName = sLastName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public void setContry(String contry) {
        this.contry = contry;
    }

    public void setTipoI(String tipoI) {
        this.tipoI = tipoI;
    }

    public void setNumI(String numI) {
        this.numI = numI;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String tipoI;
    public String numI;
    public String correo;
    public String fechaIngreso;
    public String area;
    public String estado;

    public Employe() {
    }

    @Override
    public String toString() {
        return "employe{" +
                "id=" + id +
                ", fLastName='" + fLastName + '\'' +
                ", sLastName='" + sLastName + '\'' +
                ", fName='" + fName + '\'' +
                ", sName='" + sName + '\'' +
                ", contry='" + contry + '\'' +
                ", tipoI='" + tipoI + '\'' +
                ", numI='" + numI + '\'' +
                ", correo='" + correo + '\'' +
                ", fechaIngreso='" + fechaIngreso + '\'' +
                ", area='" + area + '\'' +
                ", estado='" + estado + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employe employe = (Employe) o;
        return id == employe.id && Objects.equals(fLastName, employe.fLastName) && Objects.equals(sLastName, employe.sLastName) && Objects.equals(fName, employe.fName) && Objects.equals(sName, employe.sName) && Objects.equals(contry, employe.contry) && Objects.equals(tipoI, employe.tipoI) && Objects.equals(numI, employe.numI) && Objects.equals(correo, employe.correo) && Objects.equals(fechaIngreso, employe.fechaIngreso) && Objects.equals(area, employe.area) && Objects.equals(estado, employe.estado);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fLastName, sLastName, fName, sName, contry, tipoI, numI, correo, fechaIngreso, area, estado);
    }

    public int getId() {
        return id;
    }

    public String getfLastName() {
        return fLastName;
    }

    public String getsLastName() {
        return sLastName;
    }

    public String getfName() {
        return fName;
    }

    public String getsName() {
        return sName;
    }

    public String getContry() {
        return contry;
    }

    public String getTipoI() {
        return tipoI;
    }

    public String getNumI() {
        return numI;
    }

    public String getCorreo() {
        return correo;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public String getArea() {
        return area;
    }

    public String getEstado() {
        return estado;
    }

    public Employe(int id, String fLastName, String sLastName, String fName, String sName, String contry, String tipoI, String numI, String correo, String fechaIngreso, String area, String estado) {
        this.id = id;
        this.fLastName = fLastName;
        this.sLastName = sLastName;
        this.fName = fName;
        this.sName = sName;
        this.contry = contry;
        this.tipoI = tipoI;
        this.numI = numI;
        this.correo = correo;
        this.fechaIngreso = fechaIngreso;
        this.area = area;
        this.estado = estado;
    }
}
