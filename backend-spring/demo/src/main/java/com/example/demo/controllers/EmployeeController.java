package com.example.demo.controllers;

import com.example.demo.entities.BaseDatos;
import com.example.demo.entities.Empleado;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200",maxAge = 3600)
@RestController
@RequestMapping("/prueba")
public class EmployeeController {
    private BaseDatos database=new BaseDatos();


    @GetMapping("/empleado")
    @ApiOperation("Get all employees in db")
    @ApiResponse(code=200 ,message = "OK")
    public List<Empleado> getAllEmployee(){
        return database.getAll();
    }
    @GetMapping("empleado/{id}")
    @ApiOperation("Search a employee whit an ID")
    @ApiResponses(
            {
                    @ApiResponse(code=200,message = "OK"),
                    @ApiResponse(code=404,message = "Employee not found")
            }
    )
    public ResponseEntity<Empleado> geteEmployeById(@ApiParam(value="The id of the product" ,required = true ,example = "1") @PathVariable(value = "id") Integer Id)  {

        Empleado empleado=new Empleado();
        empleado=database.get_empleado(Id);
        return ResponseEntity.ok().body(empleado);
    }

    @PostMapping("/empleado")
    public Empleado createEmploye(@Validated @RequestBody Empleado empleado){

        database.create_empleado(empleado);
        return empleado;
    }

    @PutMapping("/empleado/{id}")
    public ResponseEntity<Empleado> updatePersona(@PathVariable(value = "id") Integer Id,
                                                  @Validated @RequestBody Empleado empleadoUpdate) {

        database.actualizar_empleado(empleadoUpdate,Id);
        return ResponseEntity.ok(empleadoUpdate);
    }

    @DeleteMapping("/empleado/{id}")
    public Map<String,Boolean> deleteEmpleado(@PathVariable(value = "id") Integer Id) {
        database.delete_empleado(Id);
        Map<String,Boolean> response = new HashMap<>();
        response.put("deleted",Boolean.TRUE);
        return response;
    }



}
